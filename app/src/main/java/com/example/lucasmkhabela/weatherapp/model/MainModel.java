package com.example.lucasmkhabela.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucasmkhabela on 2018/04/10.
 */

public class MainModel {

    @SerializedName("temp_min")
    private double tempMin;
    @SerializedName("temp_max")
    private double tempMax;

    public double getTempMin() {
        return tempMin;
    }

    public double getTempMax() {
        return tempMax;
    }

}
