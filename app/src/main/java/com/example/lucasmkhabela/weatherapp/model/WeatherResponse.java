package com.example.lucasmkhabela.weatherapp.model;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lucasmkhabela on 2018/04/03.
 */

public class WeatherResponse {

    @SerializedName("name")
    private String areaName;
    @SerializedName("weatherIcon")
    private int weatherIcon;
    @SerializedName("main")
    private MainModel mainModel;
    @SerializedName("weather")
    private List<WeatherModel> weatherModel;


    public String getAreaName() {
        return areaName;
    }

    public List<WeatherModel> getWeatherModel() {
        return weatherModel;
    }

    public MainModel getMainModel() {
        return mainModel;
    }


}
