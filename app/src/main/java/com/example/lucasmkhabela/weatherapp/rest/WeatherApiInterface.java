package com.example.lucasmkhabela.weatherapp.rest;

/**
 * Created by lucasmkhabela on 2018/04/10.
 */


import com.example.lucasmkhabela.weatherapp.model.WeatherResponse;

import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApiInterface {

    @GET("weather")
    Call<WeatherResponse> getLongitudeAndLatitudeCoordinates(@Query("lat") double latitude, @Query("lon") double longitude, @Query("units") String metric, @Query("appid") String apiKey);
}