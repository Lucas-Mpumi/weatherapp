package com.example.lucasmkhabela.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucasmkhabela on 2018/04/10.
 */

public class WeatherModel {

    @SerializedName("icon")
    private String weatherIcon;

    public String getWeatherIcon() {
        return weatherIcon;
    }
}
