package com.example.lucasmkhabela.weatherapp.activity;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lucasmkhabela.weatherapp.R;
import com.example.lucasmkhabela.weatherapp.location.GPSTracker;
import com.example.lucasmkhabela.weatherapp.validations.Validation;
import com.example.lucasmkhabela.weatherapp.constants.Constants;
import com.example.lucasmkhabela.weatherapp.model.WeatherResponse;
import com.example.lucasmkhabela.weatherapp.rest.WeatherApiClient;
import com.example.lucasmkhabela.weatherapp.rest.WeatherApiInterface;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherMainActivity extends AppCompatActivity {

    @BindView(R.id.current_time)
    TextView currentTime;
    @BindView(R.id.weather_icon)
    ImageView weatherIcon;
    @BindView(R.id.max_temperature)
    TextView maxTemperature;
    @BindView(R.id.min_temperature)
    TextView minTemperature;
    @BindView(R.id.area_name)
    TextView areaName;
    @BindView(R.id.spin_kit)
    SpinKitView spinKitView;

    GPSTracker gps;

    boolean doubleBackToExitPressedOnce = false;
    double longitude;
    double latitude;
    String units = "metric";
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_main);
        ButterKnife.bind(this);
        spinKitView.setVisibility(View.VISIBLE);
        mContext = this;
        currentTime.setText(getCurrentTimeStamp());

        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(WeatherMainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            gps = new GPSTracker(mContext, WeatherMainActivity.this);
            if (gps.canGetLocation()) {
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
            } else {
                gps.showSettingsAlert();
            }
        }

        if (Validation.isNetworkAvailable(this)) {
            initView();
        } else {
            setContentView(R.layout.no_network_available);
            findViewById(R.id.btn_retry_connect).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Validation.isNetworkAvailable(getApplication())) {
                        initView();
                    } else {
                        Toast.makeText(getApplicationContext(), "No network connection available", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    public void initView() {

        WeatherApiInterface weatherApiInterface = WeatherApiClient.getClient().create(WeatherApiInterface.class);
        Call<WeatherResponse> call = weatherApiInterface.getLongitudeAndLatitudeCoordinates(latitude, longitude, units, Constants.API_KEY);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {

                if (response.isSuccessful()) {

                    spinKitView.setVisibility(View.GONE);
                    WeatherResponse weatherResponse = response.body();

                    if (weatherResponse != null) {
                        String finalMaxTemperature = "max: " + String.valueOf(weatherResponse.getMainModel().getTempMax()) + "°C";
                        String finalMinTemperature = "min: " + String.valueOf(weatherResponse.getMainModel().getTempMin()) + "°C";
                        String finalWeatherIcon = weatherResponse.getWeatherModel().get(0).getWeatherIcon() + ".png";
                        Picasso.with(getApplicationContext())
                                .load(Constants.IMAGE_URL + finalWeatherIcon)
                                .into(weatherIcon);
                        maxTemperature.setText(finalMaxTemperature);
                        minTemperature.setText(finalMinTemperature);
                        areaName.setText(weatherResponse.getAreaName());
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "No data available", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                // Log error here since request failed
                Log.d("Error", t.getMessage());
            }
        });

    }

    @Nullable
    public static String getCurrentTimeStamp() {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            // Find today's date
            String currentDateTime = fmt.format(new Date());
            return " Today, " + currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    gps = new GPSTracker(mContext, WeatherMainActivity.this);

                    if (gps.canGetLocation()) {

                        latitude = gps.getLatitude();
                        longitude = gps.getLongitude();

                    } else {
                        gps.showSettingsAlert();
                    }

                } else {

                    Toast.makeText(mContext, "You need to grant permission", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
